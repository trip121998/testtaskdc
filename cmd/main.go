package main

import (
	"log"

	"gitlab.com/trip121998/testtaskdc/internal/app"
)

// Точка входа
func main() {
	// Создание нового приложения
	a, err := app.NewApp()
	if err != nil {
		log.Fatalf("Failed to init app: %s", err.Error())
	}

	// Запуск приложения
	err = a.Run()
	if err != nil {
		log.Fatalf("Failed to run app: %s", err.Error())
	}
}