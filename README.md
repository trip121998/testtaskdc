В файле testTaskDC.postman_collection.json хранится запрос на добавление фактов.  
Для запуска приложения введите команду:  
    - go run cmd/main.go  
Структура:  
1. cmd точка входа  
2. config содержит файл конфига (token обязательное поле)  
3. internal весь проект  
    1. app содержит файлы для создания и запуска приложения  
    2. config содержит подключение конфига  
    3. lib содержит функции облегчающий код  
    4. model содержит модели(структуры данных) которые относятся ко всему проекту  
    5. repository содержит хранилище данных  
        1. buffer хранилище данных в пямяти и управление ими  
    6. service содержит сервисы  
        1. broker сервис отвечающий за отправки данных из хранилища на сторонний api  
            1. model содержит модели которые относятся только для сервиса broker  