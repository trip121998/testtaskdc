package config

import (
	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Token 		string `yaml:"token" env-required:"true"`
	CountFacts 	int `yaml:"count_facts" env-default:"1"`
	Port 		int `yaml:"port" env-default:"3000"`
}

var cfg Config
// Путь к файлу конфига
var configPath = "config/config.yaml"

// Инициализация и подключение конфига
func MustLoad() *Config {
	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		panic("cannot read config: " + err.Error())
	}

	return &cfg
}