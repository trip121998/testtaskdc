package app

import (
	"fmt"
	"net/http"
	"gitlab.com/trip121998/testtaskdc/internal/service"
)


// Запуск HTTP сервера
func startServer(brokerService service.BrokerService, port int) error {
	portFormate := fmt.Sprintf(":%d", port)

	http.HandleFunc("/create", brokerService.CreateFact)

	fmt.Println(fmt.Sprintf("Listenning server %s", portFormate))
	err := http.ListenAndServe(portFormate, nil)

	return err
}