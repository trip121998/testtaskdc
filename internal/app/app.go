package app

import (
	"context"

	"gitlab.com/trip121998/testtaskdc/internal/repository"
	"gitlab.com/trip121998/testtaskdc/internal/service"
	"gitlab.com/trip121998/testtaskdc/internal/config"

	factRepository "gitlab.com/trip121998/testtaskdc/internal/repository/buffer/fact"
	brokerService "gitlab.com/trip121998/testtaskdc/internal/service/broker"
)

// Приложение
type App struct {
	ctx context.Context
	factRepository repository.FactRepository
	brokerService service.BrokerService
}

// Конструктор приложения
func NewApp() (App, error) {
	ctx := context.Background()
	return App{ctx: ctx}, nil
}

// Запуск приложения
func(app *App) Run() error {
	// Подключение конфига
	cfg := config.MustLoad()
	// Подключение репозитория фактов
	factRepository := app.FactRepository()
	// Подключение сервиса Брокер
	brokerService := app.BrokerService(factRepository)

	// Вызов метода по отправки запросов
	go brokerService.ApiCall(app.ctx, cfg.Token)

	// Запуск HTTP сервера
	err := startServer(brokerService, cfg.Port)

	return err
}

// Вызов репозитория факторов используя паттерн singleton
func (app *App) FactRepository() repository.FactRepository {
	if app.factRepository == nil {
		return factRepository.NewRepository()
	}

	return app.factRepository
}

// Вызов сервиса Broker используя паттерн singleton
func (app *App) BrokerService(repository repository.FactRepository) service.BrokerService {
	if app.brokerService == nil {
		return brokerService.NewService(repository)
	}

	return app.brokerService
}