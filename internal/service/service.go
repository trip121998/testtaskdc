package service

import (
	"context"
	"net/http"
)

// Интерфейс сервиса брокер для соблюдения принципа инверсии зависимости(DIP)
type BrokerService interface {
	ApiCall(ctx context.Context, token string) error
	CreateFact(res http.ResponseWriter, req *http.Request)
}