package model

import (
	"gitlab.com/trip121998/testtaskdc/internal/model"
)

type Message struct {
	Status string `json:"STATUS"`
}

type MessageFacts struct {
	Facts []model.Fact `json:"facts"`
}