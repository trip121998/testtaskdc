package broker

import (
	"gitlab.com/trip121998/testtaskdc/internal/repository"
)

// Сервис Брокер
type service struct {
	factRepository repository.FactRepository
}

// Конструктор сервиса брокер
func NewService(factRepository repository.FactRepository) service {
	return service{
		factRepository: factRepository,
	}
}