package broker

import (
	"net/http"
	"encoding/json"
	"gitlab.com/trip121998/testtaskdc/internal/service/broker/model"
)

// Handle для отправки фактов на добавление в буфер
func(s service) CreateFact(res http.ResponseWriter, req *http.Request) {
	if !validateHeaderRequest(res, req) {
		return
	}

	var messageFacts model.MessageFacts

	err := json.NewDecoder(req.Body).Decode(&messageFacts)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	go s.factRepository.Add(messageFacts.Facts)

	res.Write([]byte("OK"))
}

// Валидация заголовка запроса
func validateHeaderRequest(res http.ResponseWriter, req *http.Request) bool {
	if req.Method != "POST" {
		http.NotFound(res, req)
		return false 
	}

	contentType := req.Header.Get("Content-type")
	if contentType != "application/json" {
		res.WriteHeader(http.StatusBadRequest)
		return false
	}

	return true
}