package broker

import (
	"fmt"
	"log"
	"time"
	"context"
	"encoding/json"
	
	"gitlab.com/trip121998/testtaskdc/internal/lib/http"
	"gitlab.com/trip121998/testtaskdc/internal/service/broker/model"
)

// Метод по отправки фактов
func(s service) ApiCall(ctx context.Context, token string) error {

	for {
		facts, err := s.factRepository.GetAll()

		if err != nil {
			log.Fatalf("Failed api call: ", err)
		}

		index := len(*facts)

		for _, fact := range *facts {
			var message model.Message

			err, result := http.Request(ctx, token, fact)
			if err != nil {
				return err
			}

			// Вывод ответа для просмотра в консоли
			fmt.Println(result)

			err = json.Unmarshal([]byte(result), &message)
			if err != nil {
				return err
			}

			if message.Status != "OK" {
				return fmt.Errorf(result)
			}
			
			time.Sleep(2*time.Second)
		}

		if index != 0 {
			s.factRepository.DeleteByIndex(index)
		}

	}

	return nil
}