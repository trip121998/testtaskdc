package repository

import (
	"gitlab.com/trip121998/testtaskdc/internal/model"
)

// Интерфейс репозитория фактов для соблюдения принципа инверсии зависимости(DIP)
type FactRepository interface {
	GetAll() (result *[]model.Fact, err error)
	Add(facts []model.Fact) error
	DeleteByIndex(index int)
}