package fact

import (
	"sync"

	"gitlab.com/trip121998/testtaskdc/internal/model"
)

type repository struct {
	facts []model.Fact
	m    sync.RWMutex
}

func NewRepository() *repository {
	return &repository{
		facts: make([]model.Fact, 0),
	}
}

// Метод для добавления в буфер новых фактов
func (rep *repository) Add(facts []model.Fact) error {
	rep.m.Lock()
	defer rep.m.Unlock()

	// Создается новый слайс для того чтобы capacity не увеличивалось в геометрической прогресии
	resultSlice := make([]model.Fact, len(rep.facts), len(rep.facts) + len(facts))
	copy(resultSlice, rep.facts)

	for _, fact := range facts {
		resultSlice = append(resultSlice, model.Fact{
			PeriodStart: fact.PeriodStart,
			PeriodEnd: fact.PeriodEnd,
			PeriodKey: fact.PeriodKey,
			IndicatorToMoId: fact.IndicatorToMoId,
			IndicatorToMoFactId: fact.IndicatorToMoFactId,
			Value: fact.Value,
			FactTime: fact.FactTime,
			IsPlan: fact.IsPlan,
			AuthUserId: fact.AuthUserId,
			Comment: fact.Comment,
		})
	}

	rep.facts = resultSlice

	return nil
}

// Метод для получения всех фактов
func(rep *repository) GetAll() (result *[]model.Fact, err error) {
	return &rep.facts, nil
}

// Метод для удаление всех отправленных фактов
func (rep *repository) DeleteByIndex(index int) {
	rep.m.Lock()
	defer rep.m.Unlock()

	rep.facts = rep.facts[index:]
}