package model

import (
	"fmt"
	"bytes"
	"mime/multipart"

	"gitlab.com/trip121998/testtaskdc/internal/lib/converter"
	"gitlab.com/trip121998/testtaskdc/internal/lib/types"
)

// Модель фактов
type Fact struct {
	PeriodStart			types.CustomTime	`json:"period_start"`
	PeriodEnd			types.CustomTime	`json:"period_end"`
	PeriodKey			string				`json:"period_key"`
	IndicatorToMoId		int64				`json:"indicator_to_mo_id"`
	IndicatorToMoFactId	int32				`json:"indicator_to_mo_fact_id"`
	Value				int64				`json:"value"`
	FactTime			types.CustomTime	`json:"fact_time"`
	IsPlan				bool				`json:"is_plan"`
	AuthUserId			int32				`json:"auth_user_id"`
	Comment				string				`json:"comment"`
}



// Преобразование данных из стуктуры в form-data и получение content-type для отправки
func(fact Fact) SerializeToFormDataAndGetContentType() (*bytes.Buffer, string, error) {
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)

	writer.WriteField("period_start", fact.PeriodStart.Format("2006-01-02"))
	writer.WriteField("period_end", fact.PeriodEnd.Format("2006-01-02"))
	writer.WriteField("period_key", fmt.Sprintf("%s", fact.PeriodKey))
	writer.WriteField("indicator_to_mo_id", fmt.Sprintf("%d", fact.IndicatorToMoId))
	writer.WriteField("indicator_to_mo_fact_id", fmt.Sprintf("%d", fact.IndicatorToMoFactId))
	writer.WriteField("value", fmt.Sprintf("%d", fact.Value))
	writer.WriteField("fact_time", fact.FactTime.Format("2006-01-02"))
	writer.WriteField("is_plan", fmt.Sprintf("%d", converter.BoolToInt(fact.IsPlan)))
	writer.WriteField("auth_user_id", fmt.Sprintf("%d", fact.AuthUserId))
	writer.WriteField("comment", fmt.Sprintf("%s", fact.Comment))

	err := writer.Close()
	if err != nil {
		return nil, "", err
	}

	return payload, writer.FormDataContentType(), nil
}