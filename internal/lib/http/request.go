package http

import (
	"fmt"
	"time"
	"bytes"
	"io/ioutil"
	"net/http"
	"context"
)

type dataInterface interface {
	SerializeToFormDataAndGetContentType() (*bytes.Buffer, string, error)
}

// Функция по отправки запросов применяет в себя общий контекст, токен, и данные для отправки
func Request(
	ctx context.Context,
	token string,
	data dataInterface,
) (error, string) {
	url := "https://development.kpi-drive.ru/_api/facts/save_fact"
	method := "GET"
	payload, contentType, err := data.SerializeToFormDataAndGetContentType()

	if err != nil {
		return err, ""
	}

	client := &http.Client{}

	// Установка предельного времени ожидания от сервера через context
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	// Создание запроса
	req, err := http.NewRequestWithContext(ctx, method, url, payload)

	if err != nil {
		return err, ""
	}

	// Установка заголовков запроса
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	req.Header.Set("Content-Type", contentType)

	// Отправка запроса
	res, err := client.Do(req)

	if err != nil {
		return err, ""
	}

	defer res.Body.Close()

	// Прочитывание запроса
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err, ""
	}

	return nil, string(body)
}