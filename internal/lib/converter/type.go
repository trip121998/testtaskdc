package converter

// Функция для преобразования логического типа в числовой
func BoolToInt(val bool) int8 {
	mapBool := map[bool]int8 {
		true: 1,
		false: 0,
	}

	return mapBool[val]
}