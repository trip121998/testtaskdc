package types

import (
	"time"
)

// Новый тип времени для того чтобы распарсить json 
type CustomTime struct {
	time.Time
}

func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	ct.Time, err = time.Parse("\"2006-01-02\"", string(b))
	return
}